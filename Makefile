DESTDIR =

DATADIR = $(DESTDIR)/usr/share/games/pathological

all: write-highscores html/wheel.png

write-highscores: write-highscores.c
	gcc -s -o write-highscores write-highscores.c

html/wheel.png:
	./makehtml

install: all
	mkdir -p $(DATADIR)
	cp pathological.py $(DATADIR)/
	cp -r circuits graphics music sounds $(DATADIR)/
	rm -f $(DATADIR)/graphics/*.xcf
	rm -f $(DATADIR)/sounds/*.orig

	mkdir -p $(DESTDIR)/usr/games
	cp pathological $(DESTDIR)/usr/games/

	mkdir -p $(DESTDIR)/usr/lib/pathological/bin
	cp write-highscores $(DESTDIR)/usr/lib/pathological/bin

	# The following changes should also be performed in a post-install script
	-chgrp games $(DESTDIR)/usr/lib/pathological/bin/write-highscores
	-chmod 2755 $(DESTDIR)/usr/lib/pathological/bin/write-highscores

	mkdir -p $(DESTDIR)/usr/X11R6/include/X11/pixmaps
	cp pathological.xpm $(DESTDIR)/usr/X11R6/include/X11/pixmaps

	mkdir -p $(DESTDIR)/var/games
	cp pathological_scores $(DESTDIR)/var/games

	# The following changes should also be performed in a post-install script
	-chgrp games $(DESTDIR)/var/games/pathological_scores
	-chmod 664 $(DESTDIR)/var/games/pathological_scores

	mkdir -p $(DESTDIR)/usr/share/man/man6
	cp pathological.6.gz $(DESTDIR)/usr/share/man/man6

	mkdir -p $(DESTDIR)/usr/share/doc/pathological
	cp -r html $(DESTDIR)/usr/share/doc/pathological

uninstall:
	rm -rf $(DATADIR)
	rm -rf $(DESTDIR)/usr/lib/pathological
	rm -f $(DESTDIR)/usr/games/pathological
	rm -f $(DESTDIR)/usr/X11R6/include/X11/pixmaps/pathological.xpm
	rm -f $(DESTDIR)/usr/share/man/man6/pathological.6.gz

purge: uninstall
	rm -f $(DESTDIR)/var/games/pathological_scores

empty: distclean

distclean:
	rm -f write-highscores
	rm -f html/*.png
